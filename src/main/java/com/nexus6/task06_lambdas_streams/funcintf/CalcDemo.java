package com.nexus6.task06_lambdas_streams.funcintf;

import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CalcDemo {
  public static final Logger mainLog = LogManager.getLogger(CalcDemo.class);

  public static void main(String[] args) {
    Random rnd = new Random();
    int val1 = rnd.nextInt(100);
    int val2 = rnd.nextInt(100);
    int val3 = rnd.nextInt(100);
    Calculatable maxVal = (a, b, c) -> {int max = a;
                                        max = max > b ? max : b;
                                        max = max > c ? max : c;
                                        return max;};
    Calculatable average = (a, b, c) -> (a + b + c) / 3;
    mainLog.info(String.format("Calculating for values %d %d %d%n", val1, val2, val3) );
    mainLog.info(String.format("Max value is %d%n", maxVal.calculate(val1, val2, val3)));
    mainLog.info(String.format("Average value is %d%n", average.calculate(val1, val2, val3)));
  }
}
