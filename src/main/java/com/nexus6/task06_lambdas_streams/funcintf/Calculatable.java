package com.nexus6.task06_lambdas_streams.funcintf;
@FunctionalInterface
public interface Calculatable {
  int calculate(int a, int b, int c);
}
