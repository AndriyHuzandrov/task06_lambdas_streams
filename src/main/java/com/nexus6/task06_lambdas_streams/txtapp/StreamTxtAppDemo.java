package com.nexus6.task06_lambdas_streams.txtapp;

import com.nexus6.task06_lambdas_streams.txtapp.view.Menu;

public class StreamTxtAppDemo {

  public static void main(String[] args) {
    Menu ui = new Menu();
    ui.getTextData();

  }

}
