package com.nexus6.task06_lambdas_streams.txtapp.model;

import com.nexus6.task06_lambdas_streams.txtapp.view.Updatable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextHolder implements Publishable{
  private List<Updatable> monitors;
  private List<String> userText;

  public TextHolder() {
    userText = new LinkedList<>();
    monitors = new ArrayList<>();
  }
  public void subscribeObserver(Updatable o) {
    monitors.add(o);
  }
  public void removeObserver(Updatable o) {
    monitors = monitors
        .stream()
        .filter(obs -> obs.equals(o))
        .collect(Collectors.toList());
  }
  public void notifyObservers() {
    monitors.forEach(obs -> obs.update(this));
  }
  public void setUserText(String line) {
    userText.add(line);
  }
  public Supplier<Stream<String>> getWordsStream() {
    return () -> userText
        .stream()
        .flatMap(s -> Arrays.stream(s.split("\\p{Punct}*\\s+")));
  }
  public String getAsString() {
    return userText
        .stream()
        .collect(Collectors.joining());
  }
}
