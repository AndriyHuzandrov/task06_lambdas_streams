package com.nexus6.task06_lambdas_streams.txtapp.model;

import com.nexus6.task06_lambdas_streams.txtapp.view.Updatable;
import java.util.function.Supplier;
import java.util.stream.Stream;

public interface Publishable {

  void subscribeObserver(Updatable obs);
  void removeObserver(Updatable obs);
  void notifyObservers();
  Supplier<Stream<String>> getWordsStream();
  String getAsString();
  void setUserText(String line);

}
