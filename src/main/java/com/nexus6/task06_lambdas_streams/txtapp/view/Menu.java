package com.nexus6.task06_lambdas_streams.txtapp.view;

import static com.nexus6.task06_lambdas_streams.funcintf.CalcDemo.mainLog;
import com.nexus6.task06_lambdas_streams.txtapp.model.Publishable;
import com.nexus6.task06_lambdas_streams.txtapp.model.TextHolder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Menu {
  private BufferedReader br;
  private Publishable dataSource;
  private Map<Integer, String> txtMenu;
  private Map<String, Executable> execMenu;

  public Menu() {
    br = new BufferedReader(new InputStreamReader(System.in));
    dataSource = new TextHolder();
    txtMenu = new LinkedHashMap<>();
    execMenu = new HashMap<>();
    prepareTxtMenu();
    prepareExecMenu();
  }
  public void getTextData() {
    String inputString;
    String choice;
    mainLog.info("\nInput text or blank line to finish\n");
    try {
      do {
        inputString = br.readLine();
        if(!inputString.isEmpty()) {
          dataSource.setUserText(inputString);
        }
      } while(!inputString.isEmpty());
      txtMenu.entrySet()
          .forEach(e -> mainLog.info(e.getKey() + " " + e.getValue() + "\n"));
      execChoice(br.readLine());
      getTextData();
    } catch(IOException e) {
      mainLog.error("Error reading from console.");
    }
  }
  private void execChoice(String choice) {
    execMenu.entrySet()
        .stream()
        .filter(e -> e.getKey().compareTo(choice) == 0)
        .forEach(e -> e.getValue().execute());
  }
  private void prepareTxtMenu() {
    txtMenu.put(1, "Count the symbol occurrence");
    txtMenu.put(2, "Find unique words");
    txtMenu.put(3, "Count word occurrence");
    txtMenu.put(4, "Exit");
  }
  private void prepareExecMenu() {
    execMenu.put("1", this::showSymbolOccurrence);
    execMenu.put("2", this::showUniqueWords);
    execMenu.put("3", this::showWordOccurrence);
    execMenu.put("4", this::exitProgram);
  }
  private void showWordOccurrence() {
    Updatable monitor = new WordOccurrenceView();
    prepareObserver(monitor);
  }
  private void showUniqueWords() {
    Updatable monitor = new UniqueWordsView();
    prepareObserver(monitor);
  }
  private void exitProgram() {
    System.exit(0);
  }
  private void showSymbolOccurrence(){
    Updatable monitor = new SymbolCounterView();
    prepareObserver(monitor);
  }
  private void prepareObserver(Updatable monitor) {
    dataSource.subscribeObserver(monitor);
    dataSource.notifyObservers();
    dataSource.removeObserver(monitor);
  }
}
