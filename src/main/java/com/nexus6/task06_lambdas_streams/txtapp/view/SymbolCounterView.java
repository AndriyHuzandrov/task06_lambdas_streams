package com.nexus6.task06_lambdas_streams.txtapp.view;

import static com.nexus6.task06_lambdas_streams.funcintf.CalcDemo.mainLog;
import java.util.HashMap;
import java.util.Map;

class SymbolCounterView extends StringView {
  private Map<Character, Integer> occurrence;

  SymbolCounterView() {
    super();
    occurrence = new HashMap<>();
  }
  private void fillOccurence() {
    for(int i = 0; i < dataString.length(); i++) {
      occurrence.put(dataString.charAt(i),
          occurrence.containsKey(dataString.charAt(i)) ?
              occurrence.get(dataString.charAt(i))+1  : 1);
    }
  }
  void show() {
    fillOccurence();
    occurrence.entrySet()
        .stream()
        .sorted()
        .forEach(e -> mainLog.info(e.getKey() + " was noticed " + e.getValue() + " times\n"));
  }
}
