package com.nexus6.task06_lambdas_streams.txtapp.view;

import static com.nexus6.task06_lambdas_streams.funcintf.CalcDemo.mainLog;

import java.util.Arrays;

class UniqueWordsView extends StreamView {

  void show() {
    mainLog.info(String.format("There are %d unique words in the text%n",
                                countUniqueWords()));
    dataStream
        .get()
        .distinct()
        .map(w -> w + "\n")
        .forEach(mainLog::info);
  }
  private long countUniqueWords() {
    return dataStream
        .get()
        .distinct()
        .count();
  }
}
