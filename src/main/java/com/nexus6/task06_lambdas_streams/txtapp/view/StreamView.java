package com.nexus6.task06_lambdas_streams.txtapp.view;

import com.nexus6.task06_lambdas_streams.txtapp.model.Publishable;
import java.util.function.Supplier;
import java.util.stream.Stream;

abstract class StreamView implements Updatable {
  Supplier<Stream<String>> dataStream;

  public void update(Publishable textData) {
    dataStream = textData.getWordsStream();
    show();
  }
  abstract void show();
}
