package com.nexus6.task06_lambdas_streams.txtapp.view;

import com.nexus6.task06_lambdas_streams.txtapp.model.Publishable;

public interface Updatable {
  void update(Publishable p);
}
