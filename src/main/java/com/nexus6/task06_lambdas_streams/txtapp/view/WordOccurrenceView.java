package com.nexus6.task06_lambdas_streams.txtapp.view;

import static com.nexus6.task06_lambdas_streams.funcintf.CalcDemo.mainLog;
import java.util.function.Function;
import java.util.stream.Collectors;

class WordOccurrenceView extends StreamView {

  void show() {
    mainLog.info("Occurrences of words in the text: \n");
    dataStream
        .get()
        .sorted()
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
        .entrySet()
        .forEach(mainLog::info);
  }

}
