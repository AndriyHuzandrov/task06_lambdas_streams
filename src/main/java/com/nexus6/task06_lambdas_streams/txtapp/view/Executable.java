package com.nexus6.task06_lambdas_streams.txtapp.view;

@FunctionalInterface
public interface Executable {
  void execute();
}
