package com.nexus6.task06_lambdas_streams.txtapp.view;

import com.nexus6.task06_lambdas_streams.txtapp.model.Publishable;

abstract class StringView implements Updatable {
  String dataString;

  public void update(Publishable textData) {
    dataString = textData.getAsString();
    show();
  }
  abstract void show();
}
