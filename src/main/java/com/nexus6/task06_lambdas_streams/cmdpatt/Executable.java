package com.nexus6.task06_lambdas_streams.cmdpatt;

public interface Executable {
  void execute(String arg);
}
