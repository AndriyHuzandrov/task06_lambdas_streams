package com.nexus6.task06_lambdas_streams.cmdpatt;

import static com.nexus6.task06_lambdas_streams.funcintf.CalcDemo.mainLog;

public class StringProcessor {

  static void reverseString(String sourceString) {
    char[] tempArr = new char[sourceString.length()];
    for(int i = 0, j = tempArr.length - 1; i < tempArr.length; i++, j--) {
      tempArr[j] = sourceString.toCharArray()[i];
    }
    mainLog.info(new String(tempArr)+ "\n");
  }

  void calculateWords(String testedString) {
    String[] testArr;
    testArr = testedString.split(" ");
    mainLog.info(String.format("String has %d words%n", testArr.length));
  }

}
