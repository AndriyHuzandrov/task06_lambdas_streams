package com.nexus6.task06_lambdas_streams.cmdpatt;

import static com.nexus6.task06_lambdas_streams.funcintf.CalcDemo.mainLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Consumer;

class UI {
  private Consumer<Integer> exitApp;
  private Executable changeToUpper;
  private BufferedReader br;
  private String[] userChoiceSet;

  UI() {
    br = new BufferedReader(new InputStreamReader(System.in));
    changeToUpper = s -> mainLog.info(s.toUpperCase() + "\n");
    exitApp = n -> System.exit(n);
  }

  private void showIntro() {
    mainLog.info("Available commands are:\n");
    mainLog.info("[C]alculate words in a string\n");
    mainLog.info("Change all letters in a string to [U]pper case\n");
    mainLog.info("[M]ark the beginning and the end of a string with +\n");
    mainLog.info("[R]everse string\n");
    mainLog.info("[E]xit the program\n");
    mainLog.info("Usage: [C]ommand > string argument: ");
  }
  void getChoice() {
    String inputString;
    showIntro();
    try {
      inputString = br.readLine();
      if(inputString.compareTo("E") == 0) {
        exitApp.accept(0);
      } else if(!inputString.matches("^[CUMR] > (\\w+\\s*)+")) {
        throw new IllegalArgumentException();
      } else {
        userChoiceSet = inputString.split(" > ");
      }
    }
    catch (IllegalArgumentException e) {
      mainLog.warn("Wrong input format");
      getChoice();
    }
    catch (IOException e) {
      mainLog.error("Console reading error");
    }
    processChoice();
    getChoice();
  }
  private void processChoice() {
    switch (userChoiceSet[0]) {
      case "C":
        new CalculateWordsInString(new StringProcessor()).execute(userChoiceSet[1]);
        makePause();
        break;
      case "U":
        changeToUpper.execute(userChoiceSet[1]);
        makePause();
        break;
      case "M":
        Executable m = new Executable() {
          @Override
          public void execute(String arg) {
            mainLog.info("+ " + arg + " +\n");
          }
        };
        m.execute(userChoiceSet[1]);
        makePause();
        break;
      case "R":
        Executable r = StringProcessor::reverseString;
        r.execute(userChoiceSet[1]);
        makePause();
        break;
        default:
          break;
    }
  }
  private void makePause() {
    mainLog.info("Press any key to continue");
    try {
      br.readLine();
    } catch (IOException e) {
      mainLog.error("Console reading error");
    }
  }
}
