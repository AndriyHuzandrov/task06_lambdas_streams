package com.nexus6.task06_lambdas_streams.cmdpatt;

public class CalculateWordsInString implements Executable {
  private StringProcessor processor;

  public CalculateWordsInString(StringProcessor sProc) {
    processor = sProc;
  }

  public void execute(String testedString) {
    processor.calculateWords(testedString);
  }
}
