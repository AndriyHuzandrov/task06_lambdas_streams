package com.nexus6.task06_lambdas_streams.rndstream;

import static com.nexus6.task06_lambdas_streams.funcintf.CalcDemo.mainLog;
import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomContainer {

  public static void main(String[] args) {
    StringBuilder arrStrBuilder = new StringBuilder("Tested array: ");
    List<Integer>  testList = new Random()
        .ints(20, 0, 1000)
        .boxed()
        .collect(Collectors.toList());
    int[] testArr = IntStream
        .generate(() -> (int) (Math.random()*1000))
        .limit(20)
        .toArray();
    IntSummaryStatistics testArrStats = Arrays.stream(testArr)
                                        .peek(n -> arrStrBuilder.append(n + " "))
                                        .summaryStatistics();
    mainLog.info(arrStrBuilder + "\n");
    mainLog.info(String.format("Array Max val %d%nMin val %d%nAvg val %.2f%nSum of all elements %d%n",
                                testArrStats.getMax(),
                                testArrStats.getMin(),
                                testArrStats.getAverage(),
                                testArrStats.getSum()));
    testList.forEach(n -> mainLog.info(n + "\n"));
    int sumListElements = testList.stream()
                                  .mapToInt(Integer::intValue)
                                  .sum();
    int sumListElementReduced = testList.stream().reduce(0, (acc, n) -> acc + n);
    int minListelement = testList
        .stream()
        .min(Integer::compareTo)
        .orElseThrow(NoSuchElementException::new);
    int maxListElement = testList
        .stream()
        .sorted(Comparator.reverseOrder())
        .findFirst()
        .get();
    double avgListElement = testList
        .stream()
        .mapToInt(Integer::intValue)
        .average()
        .getAsDouble();
    mainLog.info(String.format("List Max val %d%nMin val %d%nAvg val %.2f%nSum of all elements %d%n",
                                maxListElement,
                                minListelement,
                                avgListElement,
                                sumListElements));
  }
}
